package in.g360.gaadi360areamanager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;
import com.nex3z.notificationbadge.NotificationBadge;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.g360.gaadi360areamanager.activities.ConcernListActivity;
import in.g360.gaadi360areamanager.activities.NotificationActivity;
import in.g360.gaadi360areamanager.activities.ProspectListActivity;
import in.g360.gaadi360areamanager.activities.ServiceCenterListActivity;
import in.g360.gaadi360areamanager.activities.SettingActivity;
import in.g360.gaadi360areamanager.activities.TodayHistoryActivity;
import in.g360.gaadi360areamanager.activities.TrackingActivity;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.GPSTracker;
import in.g360.gaadi360areamanager.utils.GlobalCalls;
import in.g360.gaadi360areamanager.utils.GpsUtils;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class MainActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Boolean exit = false;
    NotificationBadge badge;
    AlertDialog versionDialog;
    int version = 0;
    Typeface regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String accessToken, name, authority;
    ImageView notification_img, location_img;
    LinearLayout booking_ll, sst_ll, concern_ll, user_details_ll;
    TextView toolbar_title, share_txt, settings_txt, booking_txt, sst_txt, concern_txt, user_details_txt, location_txt, area_txt, address_txt;
    Button track_btn;

    double latitude, longitude;
    public LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider, locality;
    Location location;
    //Geocoder geocoder;
    private boolean isGPS = false;

    static final int AUTOCOMPLETE_REQUEST_CODE = 22;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    double latitute, longitiu;
    List<Address> addresses;
    Geocoder geocoder;
    String latitude1, longitude1;
    PlacesClient placesClient;
    LatLng latLng;
    String sendLat, sendLng;
    public LocationManager lm;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        name = userDetails.get(UserSessionManager.FIRST_NAME);
        authority = userDetails.get(UserSessionManager.AUTHORITY);

        Log.d("AUTHORITY", authority);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        toolbar_title.setText("Welcome \n" + name);

        share_txt = findViewById(R.id.share_img);
        share_txt.setOnClickListener(this);

        settings_txt = findViewById(R.id.settings_img);
        settings_txt.setOnClickListener(this);

        badge = findViewById(R.id.badge);
        notification_img = findViewById(R.id.notification_img);
        notification_img.setOnClickListener(this);

        checkVersionUpdate();
        getNotificationCount();

        track_btn = findViewById(R.id.track_btn);
        track_btn.setTypeface(bold);
        track_btn.setOnClickListener(this);

        if (authority.equalsIgnoreCase("CITY_MANAGER")) {
            track_btn.setVisibility(View.VISIBLE);
        } else {
            track_btn.setVisibility(View.GONE);
        }

        booking_ll = findViewById(R.id.booking_ll);
        booking_ll.setOnClickListener(this);
        sst_ll = findViewById(R.id.sst_ll);
        sst_ll.setOnClickListener(this);
        concern_ll = findViewById(R.id.concern_ll);
        concern_ll.setOnClickListener(this);
        user_details_ll = findViewById(R.id.user_details_ll);
        user_details_ll.setOnClickListener(this);

        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setTypeface(bold);
        booking_txt.setOnClickListener(this);
        sst_txt = findViewById(R.id.sst_txt);
        sst_txt.setTypeface(bold);
        sst_txt.setOnClickListener(this);
        concern_txt = findViewById(R.id.concern_txt);
        concern_txt.setTypeface(bold);
        concern_txt.setOnClickListener(this);
        user_details_txt = findViewById(R.id.user_details_txt);
        user_details_txt.setTypeface(bold);
        user_details_txt.setOnClickListener(this);
        location_txt = findViewById(R.id.location_txt);
        location_txt.setTypeface(regular);

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(regular);
        area_txt = findViewById(R.id.area_txt);
        area_txt.setTypeface(bold);

        location_img = findViewById(R.id.location_img);
        location_img.setOnClickListener(this);

    }

    private boolean checkLocationPermission() {

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    public void getCurrentLocation() {

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                getCurrentLocation();

                return;

            }
        }

        location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

        }
        geocoder = new Geocoder(MainActivity.this);

        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                latitude = addressList.get(0).getLatitude();
                longitude = addressList.get(0).getLongitude();
                locality = addressList.get(0).getAddressLine(0);

                if (!locality.isEmpty()) {

                    Log.d("LOCATION", "Locality ->" + latitude + "\n" + longitude + "\n" + locality);
                    location_txt.setText(locality);

                    sendLocation();

                } else {

                    GlobalCalls.showToast("Something went wrong..!", MainActivity.this);

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendLocation() {

        String url = AppUrls.BASE_URL + AppUrls.SEND_LOCATION;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("latitude", latitute);
            jsonObject.put("longitude", longitiu);
            jsonObject.put("location", locality);

        } catch (Exception e) {

        }

        Log.d("JSONOBJ", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    flipProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(request);

    }

    private void checkVersionUpdate() {

        try {
            PackageInfo pInfo = MainActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            Log.d("VERSION", "" + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String url = AppUrls.BASE_URL + AppUrls.VERSION_UPDATE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.optString("id");
                    String platformId = jsonObject.optString("platformId");
                    int currentVersionCode = jsonObject.optInt("currentVersionCode");
                    int mandatoryUpdateVersionCode = jsonObject.optInt("mandatoryUpdateVersionCode");
                    String versionName = jsonObject.optString("versionName");
                    String mandatory = jsonObject.optString("mandatory");
                    String status = jsonObject.optString("status");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");

                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert();
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        GlobalCalls.showToast("Connection Not Found..!", MainActivity.this);

                    } else if (error instanceof AuthFailureError) {

                        GlobalCalls.showToast("User Not Found..!", MainActivity.this);

                    } else if (error instanceof ServerError) {

                        GlobalCalls.showToast("Server Not Found..!", MainActivity.this);


                    } else if (error instanceof NetworkError) {

                        GlobalCalls.showToast("Network Not Found..!", MainActivity.this);

                    } else if (error instanceof ParseError) {

                        GlobalCalls.showToast("Please Try Later..!", MainActivity.this);

                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    public void versionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout);
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    public void optionalVersionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    private void getNotificationCount() {

        String url = AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.length() != 0) {

                        badge.setVisibility(View.VISIBLE);

                        String notificationUnreadCount = jsonObject.optString("notificationUnreadCount");
                        badge.setNumber(Integer.parseInt(notificationUnreadCount));

                    } else {
                        badge.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        if (v == share_txt) {

            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "A two-wheeler service that's sure to give you more SMILEAGE.\n" +
                        "Enjoy unparalleled service with Gaadi360.  \n" +
                        "Download at app.gaadi360.com");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == settings_txt) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == notification_img) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == track_btn) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, TrackingActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == booking_ll || v == booking_txt) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, TodayHistoryActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == concern_ll || v == concern_txt) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, ConcernListActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == sst_ll || v == sst_txt) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, ServiceCenterListActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == user_details_ll || v == user_details_txt) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, ProspectListActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == location_img) {

            getCurrentLocation();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        checkLocationPermission();

        flipProgressDialog.show(getFragmentManager(), "");
        flipProgressDialog.setCancelable(false);

        /*new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {

                isGPS = isGPSEnable;

                getCurrentLocation();
            }
        });*/

        /*GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            String stringLatitude = String.valueOf(gpsTracker.latitude);
            String stringLongitude = String.valueOf(gpsTracker.longitude);
            String country = gpsTracker.getCountryName(this);
            String city = gpsTracker.getLocality(this);
            String postalCode = gpsTracker.getPostalCode(this);
            String addressLine = gpsTracker.getAddressLine(this);

            location_txt.setText(stringLatitude + "\n" + stringLongitude + "\n" + addressLine + "\n" + city + "\n" + country + "\n" + postalCode);
        } else {

            gpsTracker.showSettingsAlert();
        }*/

        String apiKey = getString(R.string.googleapikey);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        placesClient = Places.createClient(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });

        configureCameraIdle();

    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);

        } else {

            GlobalCalls.showToast("Press Back again to Exit.!", MainActivity.this);

            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    private void configureCameraIdle() {

        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                latLng = mMap.getCameraPosition().target;

                try {
                    Geocoder geocoder = new Geocoder(MainActivity.this);
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        locality = addressList.get(0).getAddressLine(0);
                        String addr[] = locality.split(",", 2);

                        sendLat = String.valueOf(latLng.latitude);
                        sendLng = String.valueOf(latLng.longitude);

                        String country = addressList.get(0).getCountryName();
                        String state = addressList.get(0).getAdminArea();
                        String city = addressList.get(0).getLocality();
                        String area = addressList.get(0).getSubLocality();
                        String pincode = addressList.get(0).getPostalCode();
                        latitute = addressList.get(0).getLatitude();
                        longitiu = addressList.get(0).getLongitude();

                        if (area != null) {
                            area_txt.setText(area);
                        } else {
                            area_txt.setText("Select Location");
                        }

                        address_txt.setText(locality);

                        Log.d("LOCATION1", latLng.latitude + "\n" + latLng.longitude + "\n" + locality);

                        if (!locality.isEmpty()) {
                            Log.d("LOCALITY", locality);
                            sendLocation();
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraIdleListener(onCameraIdleListener);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

        Log.d("LOCATION2", latLng.latitude + "\n" + latLng.longitude + "\n" + locality);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();

                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude1 = latValue;
                String lngValue = stringlat.split(",")[1];
                longitude1 = lngValue;

                sendLat = latitude1;
                sendLng = longitude1;

                Double lat = Double.valueOf(latitude1);
                Double lng = Double.valueOf(longitude1);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);

                try {
                    addresses = geocoder.getFromLocation(lat, lng, 1);

                    Log.d("LOXXX", addresses.get(0).getAddressLine(0));
                    String addLine = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String area = addresses.get(0).getSubLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String addLineTwo = addresses.get(0).getThoroughfare();
                    String addLineOne = addresses.get(0).getFeatureName();

                    if (area != null) {
                        if (addr[1] != null && area != null && addr[1].contains(area)) {
                            addr[1] = addr[1].replace(area, " ");
                        }
                    }

                    if (city != null) {
                        if (addr[1].contains(city)) {
                            addr[1] = addr[1].replace(city, " ");
                        }
                    }

                    if (state != null) {
                        if (addr[1].contains(state)) {
                            addr[1] = addr[1].replace(state, " ");
                        }
                    }

                    if (postalCode != null) {
                        if (addr[1].contains(postalCode)) {
                            addr[1] = addr[1].replace(postalCode, " ");
                        }
                    }

                    if (country != null) {
                        if (addr[1].contains(country)) {
                            addr[1] = addr[1].replace(country, " ");
                        }
                    }

                    location_txt.setText(area);
                    address_txt.setText(addr[1]);

                    addr[1] = new LinkedHashSet<String>(Arrays.asList(addr[1].split("\\s"))).toString().replaceAll("[\\[\\],]", "");

                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    LatLng latLng = new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(19));
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d("LOCATION3", latLng.latitude + "\n" + latLng.longitude + "\n" + locality);

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                GlobalCalls.showToast(status.getStatusMessage(), MainActivity.this);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }
}
