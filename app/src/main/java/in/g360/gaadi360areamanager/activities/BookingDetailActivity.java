package in.g360.gaadi360areamanager.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.g360.gaadi360areamanager.MainActivity;
import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.adapters.AddonAdapter;
import in.g360.gaadi360areamanager.adapters.InspAdapter;
import in.g360.gaadi360areamanager.adapters.PhotosAdapter;
import in.g360.gaadi360areamanager.adapters.ServicesAdapter;
import in.g360.gaadi360areamanager.models.BookingServicesModel;
import in.g360.gaadi360areamanager.models.InspectionModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class BookingDetailActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ImageView close, redirect_img;
    private boolean checkInternet;
    String bookingId, accessToken, authority, brandId, brandName, modelId, modelName, status, startRange, endRange, mobile, sstMobileNumber,
            serviceCenterId, userId, activity, pickupAddressLatitude, pickupAddressLongitude;
    UserSessionManager userSessionManager;
    TextView toolbar_title, sst_name_txt, booking_txt, brand_txt, model_txt, date_txt, time_txt, addon_service_txt, pick_date_txt, pick_time_txt,
            photos_txt, payment_status_txt, pickup_txt, final_price_txt, bike_txt, service_txt, address_txt, est_price_txt, est_txt, tot_txt,
            init_txt, init_price_txt, disc_txt, dicount_txt, addon_txt, addon_price_txt, insp_txt, remarks_txt, remark_txt, moving_txt,
            kilometers_txt, customer_mobile_txt, reg_txt, customer_txt, sst_txt, sst_mobile_txt;
    Typeface regular, bold;
    TableRow discount_tr, addon_tr;
    RelativeLayout services_rl, loc_rl, photo_rl, addon_rl, insp_rl, remarks_rl;

    RecyclerView services_recyclerview, addon_recyclerview, photos_recyclerview, insp_recyclerview;
    ServicesAdapter servicesAdapter;
    List<BookingServicesModel> servicesModels = new ArrayList<BookingServicesModel>();
    AddonAdapter addonAdapter;
    List<BookingServicesModel> addOnModels = new ArrayList<BookingServicesModel>();
    InspAdapter inspAdapter;
    List<InspectionModel> inspectionModels = new ArrayList<InspectionModel>();
    PhotosAdapter photosAdapter;
    List<String> mylist = new ArrayList<String>();

    private SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);
        authority = userDetails.get(UserSessionManager.AUTHORITY);

        activity = getIntent().getStringExtra("activity");
        bookingId = getIntent().getStringExtra("bookingId");


        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        swipe = findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange, R.color.colorPrimary);
        swipe.setOnRefreshListener(this);

        redirect_img = findViewById(R.id.redirect_img);
        redirect_img.setOnClickListener(this);

        sst_name_txt = findViewById(R.id.sst_name_txt);
        sst_name_txt.setTypeface(bold);

        TextPaint paint = sst_name_txt.getPaint();
        Shader textShader = new LinearGradient(0, 0, 0, sst_name_txt.getTextSize(),
                new int[]{
                        Color.parseColor("#003171"),
                        Color.parseColor("#FF8000")
                }, null, Shader.TileMode.CLAMP);
        sst_name_txt.getPaint().setShader(textShader);
        sst_name_txt.setTextColor(Color.parseColor("#003171"));

        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setTypeface(bold);
        customer_txt = findViewById(R.id.customer_txt);
        customer_txt.setTypeface(bold);
        customer_mobile_txt = findViewById(R.id.customer_mobile_txt);
        customer_mobile_txt.setTypeface(bold);
        customer_mobile_txt.setPaintFlags(customer_mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        customer_mobile_txt.setOnClickListener(this);
        sst_txt = findViewById(R.id.sst_txt);
        sst_txt.setTypeface(bold);
        sst_mobile_txt = findViewById(R.id.sst_mobile_txt);
        sst_mobile_txt.setTypeface(bold);
        sst_mobile_txt.setPaintFlags(sst_mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        sst_mobile_txt.setOnClickListener(this);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        reg_txt = findViewById(R.id.reg_txt);
        reg_txt.setTypeface(regular);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);

        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);

        remarks_txt = findViewById(R.id.remarks_txt);
        remarks_txt.setTypeface(bold);
        remark_txt = findViewById(R.id.remark_txt);
        remark_txt.setTypeface(regular);

        addon_service_txt = findViewById(R.id.addon_service_txt);
        addon_service_txt.setTypeface(bold);

        services_recyclerview = findViewById(R.id.services_recyclerview);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getApplicationContext());
        services_recyclerview.setLayoutManager(layoutManager2);
        servicesAdapter = new ServicesAdapter(servicesModels, BookingDetailActivity.this, R.layout.row_addon);

        insp_recyclerview = findViewById(R.id.insp_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        insp_recyclerview.setLayoutManager(layoutManager);
        inspAdapter = new InspAdapter(inspectionModels, BookingDetailActivity.this, R.layout.row_insp);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager1);
        addonAdapter = new AddonAdapter(addOnModels, BookingDetailActivity.this, R.layout.row_addon);

        insp_rl = findViewById(R.id.insp_rl);
        remarks_rl = findViewById(R.id.remarks_rl);
        services_rl = findViewById(R.id.services_rl);
        addon_rl = findViewById(R.id.addon_rl);
        photo_rl = findViewById(R.id.photo_rl);
        loc_rl = findViewById(R.id.loc_rl);
        loc_rl.setOnClickListener(this);

        photos_recyclerview = findViewById(R.id.photos_recyclerview);
        photosAdapter = new PhotosAdapter(mylist, BookingDetailActivity.this, R.layout.row_photos);
        photos_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(bold);
        pick_date_txt = findViewById(R.id.pick_date_txt);
        pick_date_txt.setTypeface(regular);
        pick_time_txt = findViewById(R.id.pick_time_txt);
        pick_time_txt.setTypeface(regular);
        pickup_txt = findViewById(R.id.pickup_txt);
        pickup_txt.setTypeface(regular);
        pickup_txt.setOnClickListener(this);

        discount_tr = findViewById(R.id.discount_tr);
        addon_tr = findViewById(R.id.addon_tr);

        est_txt = findViewById(R.id.est_txt);
        est_txt.setTypeface(bold);
        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(bold);

        init_txt = findViewById(R.id.init_txt);
        init_txt.setTypeface(bold);
        init_price_txt = findViewById(R.id.init_price_txt);
        init_price_txt.setTypeface(bold);

        disc_txt = findViewById(R.id.disc_txt);
        disc_txt.setTypeface(bold);
        dicount_txt = findViewById(R.id.dicount_txt);
        dicount_txt.setTypeface(bold);

        insp_txt = findViewById(R.id.insp_txt);
        insp_txt.setTypeface(bold);
        addon_txt = findViewById(R.id.addon_txt);
        addon_txt.setTypeface(bold);
        addon_price_txt = findViewById(R.id.addon_price_txt);
        addon_price_txt.setTypeface(bold);

        tot_txt = findViewById(R.id.tot_txt);
        tot_txt.setTypeface(bold);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);

        photos_txt = findViewById(R.id.photos_txt);
        photos_txt.setTypeface(bold);

        payment_status_txt = findViewById(R.id.payment_status_txt);
        payment_status_txt.setTypeface(bold);

        moving_txt = findViewById(R.id.moving_txt);
        moving_txt.setTypeface(bold);

        kilometers_txt = findViewById(R.id.kilometers_txt);
        kilometers_txt.setTypeface(regular);

        getHistoryDetail();
    }

    private void getHistoryDetail() {

        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("RESP", response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String bookingId = jsonObject.optString("bookingId");
                    booking_txt.setText("Booking Id : " + bookingId);
                    userId = jsonObject.optString("userId");
                    String orderId = jsonObject.optString("orderId");
                    serviceCenterId = jsonObject.optString("serviceCenterId");
                    String bookingType = jsonObject.optString("bookingType");
                    brandId = jsonObject.optString("brandId");
                    brandName = jsonObject.optString("brandName");
                    modelId = jsonObject.optString("modelId");
                    modelName = jsonObject.optString("modelName");
                    String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                    String bookingDate = jsonObject.optString("bookingDate");
                    String bookingTime = jsonObject.optString("bookingTime");
                    String pickupAddress = jsonObject.optString("pickupAddress");
                    pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                    pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                    String pickedUpTime = jsonObject.optString("pickedUpTime");
                    String deliveredTime = jsonObject.optString("deliveredTime");
                    String addressType = jsonObject.optString("addressType");
                    String estimatedCost = jsonObject.optString("estimatedCost");
                    String finalPrice = jsonObject.optString("finalPrice");
                    String inspectionWords = jsonObject.optString("inspectionWords");
                    String promocodeAmount = jsonObject.optString("promocodeAmount");
                    String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                    String paymentStatus = jsonObject.optString("paymentStatus");
                    Boolean movingCondition = jsonObject.optBoolean("movingCondition");

                    if (movingCondition) {
                        moving_txt.setTextColor(ContextCompat.getColor(BookingDetailActivity.this, R.color.green));
                        moving_txt.setText("Vehicle Is In Moving Condition");
                    } else {
                        moving_txt.setTextColor(ContextCompat.getColor(BookingDetailActivity.this, R.color.red));
                        moving_txt.setText("Vehicle Is Not In Moving Condition");
                    }

                    if (paymentStatus.equalsIgnoreCase("2")) {
                        payment_status_txt.setText("Final Payment Done");
                    } else {
                        payment_status_txt.setTextColor(Color.RED);
                        payment_status_txt.setText("Final Payment Pending");
                    }

                    status = jsonObject.optString("status");
                    String deleted = jsonObject.optString("deleted");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");
                    String firstName = jsonObject.optString("firstName");
                    String lastName = jsonObject.optString("lastName");
                    String serviceCenterName = jsonObject.optString("serviceCenterName");
                    sst_name_txt.setText(serviceCenterName);
                    sstMobileNumber = jsonObject.optString("contactMobileNumber");
                    mobile = jsonObject.optString("mobile");
                    String rescheduleCount = jsonObject.optString("rescheduleCount");
                    String paidAmount = jsonObject.optString("paidAmount");
                    String addonsAmount = jsonObject.optString("addonsAmount");
                    String registrationNumber = jsonObject.optString("registrationNumber");
                    reg_txt.setText(registrationNumber);

                    customer_mobile_txt.setText(mobile);
                    sst_mobile_txt.setText(sstMobileNumber);

                    if (inspectionWords.isEmpty()) {
                        remarks_rl.setVisibility(View.GONE);
                    } else {
                        remarks_rl.setVisibility(View.VISIBLE);
                        remark_txt.setText(inspectionWords);
                    }

                    if (jsonObject.has("fileIds")) {

                        photos_txt.setVisibility(View.VISIBLE);
                        photo_rl.setVisibility(View.VISIBLE);
                        photos_recyclerview.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("fileIds");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            String data = jsonArray.get(i).toString();
                            mylist.add(AppUrls.IMAGE_URL + data);
                        }

                        Log.d("PHOTOS", String.valueOf(mylist));

                        photos_recyclerview.setAdapter(photosAdapter);
                        photosAdapter.notifyDataSetChanged();

                    } else {
                        photos_txt.setVisibility(View.GONE);
                        photo_rl.setVisibility(View.GONE);
                        photos_recyclerview.setVisibility(View.GONE);
                    }

                    if (jsonObject.has("services")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("services");

                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                BookingServicesModel servicesModel = new BookingServicesModel();
                                servicesModel.setServiceId(jsonObject1.optString("serviceId"));
                                servicesModel.setCost(jsonObject1.optString("cost"));
                                servicesModel.setServiceName(jsonObject1.optString("serviceName"));
                                servicesModel.setStatus(jsonObject1.optString("status"));
                                servicesModels.add(servicesModel);
                            }

                            services_recyclerview.setAdapter(servicesAdapter);
                            servicesAdapter.notifyDataSetChanged();
                        } else {
                            services_rl.setVisibility(View.GONE);
                        }

                    }

                    if (jsonObject.has("inspections")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                        if (jsonArray.length() > 0) {

                            insp_rl.setVisibility(View.VISIBLE);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                InspectionModel inspectionModel = new InspectionModel();
                                inspectionModel.setInspectionId(jsonObject1.optString("inspectionId"));
                                inspectionModel.setBookingId(jsonObject1.optString("bookingId"));
                                inspectionModel.setState(jsonObject1.optString("state"));
                                inspectionModel.setInspectionName(jsonObject1.optString("inspectionName"));
                                inspectionModels.add(inspectionModel);
                            }

                            insp_recyclerview.setAdapter(inspAdapter);
                            inspAdapter.notifyDataSetChanged();
                        } else {
                            insp_rl.setVisibility(View.GONE);
                        }

                    }

                    if (jsonObject.has("bookingAddonServices")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                BookingServicesModel servicesModel = new BookingServicesModel();
                                servicesModel.setServiceId(jsonObject1.optString("serviceId"));
                                servicesModel.setCost(jsonObject1.optString("cost"));
                                servicesModel.setServiceName(jsonObject1.optString("serviceName"));
                                servicesModel.setStatus(jsonObject1.optString("status"));
                                addOnModels.add(servicesModel);
                            }

                            addon_recyclerview.setAdapter(addonAdapter);
                            addonAdapter.notifyDataSetChanged();
                        } else {
                            addon_rl.setVisibility(View.GONE);
                        }

                    }

                    if (jsonObject.has("kilometerRange")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                        startRange = jsonObject1.optString("startRange");
                        endRange = jsonObject1.optString("endRange");

                        kilometers_txt.setText(startRange + " - " + endRange + " km");
                    }

                    if (pickedUpTime.isEmpty()) {
                        pick_date_txt.setVisibility(View.GONE);
                    } else {
                        pick_date_txt.setVisibility(View.VISIBLE);
                        pick_date_txt.setText("Pick Up : " + pickedUpTime);
                    }

                    if (deliveredTime.isEmpty()) {
                        pick_time_txt.setVisibility(View.GONE);
                    } else {
                        pick_time_txt.setVisibility(View.VISIBLE);
                        pick_time_txt.setText("Delivery : " + deliveredTime);
                    }

                    String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                    Log.d("ResultDate", resultDate);

                    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                    Date d = null;
                    try {
                        d = df.parse(bookingTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.HOUR, 5);
                    cal.add(Calendar.MINUTE, 30);
                    String newTime = df.format(cal.getTime());

                    try {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                        Date date = dateFormatter.parse(newTime);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                        //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                        newTime = timeFormatter.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    brand_txt.setText(brandName);
                    model_txt.setText(modelName);
                    date_txt.setText(resultDate);
                    time_txt.setText(newTime);

                    pickup_txt.setText(pickupAddress);

                    if (addonsAmount.equalsIgnoreCase("0")) {
                        addon_tr.setVisibility(View.GONE);
                    } else {
                        addon_tr.setVisibility(View.VISIBLE);
                        addon_price_txt.setText("\u20B9 " + addonsAmount);
                    }
                    if (promocodeAmount.isEmpty() || promocodeAmount.equalsIgnoreCase("0")) {
                        discount_tr.setVisibility(View.GONE);
                    } else {
                        discount_tr.setVisibility(View.VISIBLE);
                        dicount_txt.setText("\u20B9 " + promocodeAmount);
                    }

                    est_price_txt.setText("\u20B9 " + estimatedCost);
                    init_price_txt.setText("\u20B9 " + initialPaidAmount);
                    final_price_txt.setText("\u20B9 " + paidAmount);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                if (activity.equalsIgnoreCase("NewHistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, TodayHistoryActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("HistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, UpcomingHistoryActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("DeliveredHistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, DeliveredActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("NotificationAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, NotificationActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("ConcernAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, ConcernListActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == customer_mobile_txt) {
            if (checkInternet) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(mobile)));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == sst_mobile_txt) {
            if (checkInternet) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(sstMobileNumber)));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == loc_rl || v == pickup_txt || v == redirect_img) {
            if (checkInternet) {

                String uri = "http://maps.google.com/maps?q=loc:" + pickupAddressLatitude + "," + pickupAddressLongitude + " (" + "Customer" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

    }

    @Override
    public void onBackPressed() {

        if (activity.equalsIgnoreCase("NewHistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, TodayHistoryActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("HistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, UpcomingHistoryActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("DeliveredHistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, DeliveredActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("NotificationAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, NotificationActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("ConcernAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, ConcernListActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
            startActivity(intent);
        }

    }


    @Override
    public void onRefresh() {

        swipe.setRefreshing(false);

        Intent intent = new Intent(BookingDetailActivity.this, BookingDetailActivity.class);
        intent.putExtra("activity", "BookingDetailActivity");
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);

    }
}
