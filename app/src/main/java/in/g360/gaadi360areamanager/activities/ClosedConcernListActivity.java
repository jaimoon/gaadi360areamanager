package in.g360.gaadi360areamanager.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.gaadi360areamanager.MainActivity;
import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.adapters.ConcernAdapter;
import in.g360.gaadi360areamanager.models.ConcernModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class ClosedConcernListActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close, no_data_img;
    TextView toolbar_title;
    RecyclerView concern_recyclerview;
    Typeface regular, bold;

    UserSessionManager userSessionManager;
    String accessToken;
    SearchView concern_search;

    ConcernAdapter concernAdapter;
    ArrayList<ConcernModel> concernModels = new ArrayList<ConcernModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closed_concern_list);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        setupBottomBar();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        checkInternet = NetworkChecking.isConnected(this);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        no_data_img = findViewById(R.id.no_data_img);

        concern_recyclerview = findViewById(R.id.concern_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        concern_recyclerview.setLayoutManager(layoutManager);
        concernAdapter = new ConcernAdapter(concernModels, ClosedConcernListActivity.this, R.layout.row_concern);

        getConcerns();

        concern_search = findViewById(R.id.concern_search);
        EditText searchEditText = concern_search.findViewById(androidx.appcompat.R.id.search_src_text);
        concern_search.setOnClickListener(v -> concern_search.setIconified(false));
        concern_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                concernAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getConcerns() {

        String url = AppUrls.BASE_URL + AppUrls.CONCERNS + "2";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                concernModels.clear();

                try {

                    JSONArray jsonArray = new JSONArray(response);

                    if (jsonArray.length() != 0) {

                        no_data_img.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            ConcernModel cm = new ConcernModel();
                            cm.setServiceCenterName(jsonObject.optString("serviceCenterName"));
                            cm.setConcernId(jsonObject.optString("concernId"));
                            cm.setBookingId(jsonObject.optString("bookingId"));
                            cm.setConcernMessage(jsonObject.optString("concernMessage"));
                            cm.setBookingDate(jsonObject.optString("bookingDate"));
                            cm.setBookingTime(jsonObject.optString("bookingTime"));
                            cm.setDeliveredTime(jsonObject.optString("deliveredTime"));
                            cm.setConcernTime(jsonObject.optString("concernTime"));
                            cm.setUserMobile(jsonObject.optString("userMobile"));
                            cm.setUserName(jsonObject.optString("userName"));
                            cm.setFinalPrice(jsonObject.optString("finalPrice"));
                            cm.setStatus(jsonObject.optString("status"));
                            cm.setResolvedMessage(jsonObject.optString("resolvedMessage"));

                            concernModels.add(cm);
                        }

                        concern_recyclerview.setAdapter(concernAdapter);
                        concernAdapter.notifyDataSetChanged();
                    } else {
                        no_data_img.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ClosedConcernListActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(ClosedConcernListActivity.this, ConcernListActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void setupBottomBar() {
        BottomNavigationBar bottomNavigationBar = findViewById(R.id.bottom_bar);

        BottomBarItem home = new BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.open);
        BottomBarItem history = new BottomBarItem(R.drawable.ic_diploma, R.string.closed);

        bottomNavigationBar
                .addTab(home)
                .addTab(history);

        bottomNavigationBar.selectTab(1, true);

        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                showContent(position);
            }
        });
    }

    void showContent(int position) {

        if (position == 0) {
            Intent intent = new Intent(ClosedConcernListActivity.this, ConcernListActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ClosedConcernListActivity.this, ConcernListActivity.class);
        startActivity(intent);
    }
}
