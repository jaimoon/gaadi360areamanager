package in.g360.gaadi360areamanager.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.g360.gaadi360areamanager.MainActivity;
import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.adapters.ProspectListAdapter;
import in.g360.gaadi360areamanager.models.ProspectListModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.GlobalCalls;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class ProspectListActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close, add_img;
    TextView toolbar_title;
    Typeface regular, bold;
    UserSessionManager userSessionManager;
    SearchView prospect_search;
    RecyclerView prospect_recyclerview;
    ProspectListAdapter prospectListAdapter;
    ArrayList<ProspectListModel> prospectListModels = new ArrayList<ProspectListModel>();
    String accessToken;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    EditText mobile_edt, first_name_edt, last_name_edt, email_edt, message_edt;
    TextView date_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospect_list);

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        add_img = findViewById(R.id.add_img);
        add_img.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        prospect_recyclerview = findViewById(R.id.prospect_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        prospect_recyclerview.setLayoutManager(layoutManager);
        prospectListAdapter = new ProspectListAdapter(prospectListModels, ProspectListActivity.this, R.layout.row_prospects);

        getProspectList();

        prospect_search = findViewById(R.id.prospect_search);
        EditText searchEditText = prospect_search.findViewById(androidx.appcompat.R.id.search_src_text);
        prospect_search.setOnClickListener(v -> prospect_search.setIconified(false));
        prospect_search.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                //serviceCenterListAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getProspectList() {

        String url = AppUrls.BASE_URL + AppUrls.PROSPECTS_LIST;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                prospectListModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.optJSONObject("data");

                        JSONArray jsonArray = jsonObject1.optJSONArray("prospects");

                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                ProspectListModel plm = new ProspectListModel();
                                plm.setId(jsonObject2.optString("id"));
                                plm.setUsername(jsonObject2.optString("username"));
                                plm.setFirstName(jsonObject2.optString("firstName"));
                                plm.setLastName(jsonObject2.optString("lastName"));
                                plm.setBookingDate(jsonObject2.optString("bookingDate"));
                                plm.setComments(jsonObject2.optString("comments"));
                                plm.setEmail(jsonObject2.optString("email"));
                                plm.setCreatedTime(jsonObject2.optString("createdTime"));
                                plm.setAreaManagerName(jsonObject2.optString("areaManagerName"));
                                plm.setAreaManagerNumber(jsonObject2.optString("areaManagerNumber"));
                                plm.setType(jsonObject2.optString("type"));
                                prospectListModels.add(plm);
                            }

                            prospect_recyclerview.setAdapter(prospectListAdapter);
                            prospectListAdapter.notifyDataSetChanged();

                        } else {
                            GlobalCalls.showToast("No Data Available", ProspectListActivity.this);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProspectListActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(ProspectListActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == add_img) {
            if (checkInternet) {

                showProspectDialog(R.layout.prospect_dialog);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void showProspectDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ProspectListActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView prospect_title = layoutView.findViewById(R.id.prospect_title);
        prospect_title.setTypeface(bold);

        mobile_edt = layoutView.findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(regular);
        first_name_edt = layoutView.findViewById(R.id.first_name_edt);
        first_name_edt.setTypeface(regular);
        last_name_edt = layoutView.findViewById(R.id.last_name_edt);
        last_name_edt.setTypeface(regular);
        email_edt = layoutView.findViewById(R.id.email_edt);
        email_edt.setTypeface(regular);
        date_txt = layoutView.findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        message_edt = layoutView.findViewById(R.id.message_edt);
        message_edt.setTypeface(regular);

        date_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDate();
            }
        });

        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);

        Button add_btn = layoutView.findViewById(R.id.add_btn);
        add_btn.setTypeface(bold);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mobile_edt.getText().toString().isEmpty()) {

                    mobile_edt.setError("Please Enter Mobile Number");

                } else if (first_name_edt.getText().toString().isEmpty()) {

                    first_name_edt.setError("Please Enter First Name");

                }
                if (date_txt.getText().toString().equalsIgnoreCase("Select Booking Date")) {

                    date_txt.setError("Please Select Date");

                } else {
                    addProspects();

                }

            }
        });

        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    public void getDate() {

        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        date_txt.setText(strDate);

                    }

                }, year, month, day);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void addProspects() {

        String url = AppUrls.BASE_URL + AppUrls.ADD_PROSPECT;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("username", mobile_edt.getText().toString().trim());
            jsonObj.put("firstName", first_name_edt.getText().toString());
            jsonObj.put("lastName", last_name_edt.getText().toString());
            jsonObj.put("bookingDate", date_txt.getText().toString());
            jsonObj.put("comments", message_edt.getText().toString());
            jsonObj.put("email", email_edt.getText().toString());

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("ProspectListActivity", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {
                        alertDialog.dismiss();
                        GlobalCalls.showToast(message, ProspectListActivity.this);
                        getProspectList();
                    }

                    if (status.equalsIgnoreCase("5001")) {

                        GlobalCalls.showToast(message, ProspectListActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProspectListActivity.this);
        requestQueue.add(request);
    }
}
