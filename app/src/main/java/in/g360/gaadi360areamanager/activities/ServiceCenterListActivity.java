package in.g360.gaadi360areamanager.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.g360.gaadi360areamanager.MainActivity;
import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.adapters.ServiceCenterListAdapter;
import in.g360.gaadi360areamanager.models.ServiceCenterListModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.GlobalCalls;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class ServiceCenterListActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close;
    TextView toolbar_title;
    Typeface regular, bold;
    UserSessionManager userSessionManager;
    SearchView sst_search;
    RecyclerView sst_recyclerview;
    ServiceCenterListAdapter serviceCenterListAdapter;
    ArrayList<ServiceCenterListModel> serviceCenterListModels = new ArrayList<ServiceCenterListModel>();
    String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_center_list);

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        sst_recyclerview = findViewById(R.id.sst_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        sst_recyclerview.setLayoutManager(layoutManager);
        serviceCenterListAdapter = new ServiceCenterListAdapter(serviceCenterListModels, ServiceCenterListActivity.this, R.layout.row_sst);

        getSstList();

        sst_search = findViewById(R.id.sst_search);
        EditText searchEditText = sst_search.findViewById(androidx.appcompat.R.id.search_src_text);
        sst_search.setOnClickListener(v -> sst_search.setIconified(false));
        sst_search.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                serviceCenterListAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getSstList() {

        String url = AppUrls.BASE_URL + AppUrls.SST_LIST;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                serviceCenterListModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.optJSONObject("data");

                        JSONArray jsonArray = jsonObject1.optJSONArray("serviceCenters");

                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                ServiceCenterListModel sclm = new ServiceCenterListModel();
                                sclm.setServiceCenterId(jsonObject2.optString("serviceCenterId"));
                                sclm.setServiceCenterName(jsonObject2.optString("serviceCenterName"));
                                sclm.setContactName(jsonObject2.optString("contactName"));
                                sclm.setUserId(jsonObject2.optString("userId"));
                                sclm.setContactMobileNumber(jsonObject2.optString("contactMobileNumber"));
                                sclm.setContactEmail(jsonObject2.optString("contactEmail"));
                                sclm.setAddress(jsonObject2.optString("address"));
                                sclm.setLatitude(jsonObject2.optString("latitude"));
                                sclm.setLongitude(jsonObject2.optString("longitude"));
                                sclm.setOpenTime(jsonObject2.optString("openTime"));
                                sclm.setCloseTime(jsonObject2.optString("closeTime"));
                                sclm.setAverageRating(jsonObject2.optString("averageRating"));
                                sclm.setDeleted(jsonObject2.optString("deleted"));
                                sclm.setCreatedTime(jsonObject2.optString("createdTime"));
                                sclm.setModifiedTime(jsonObject2.optString("modifiedTime"));
                                sclm.setDisplayTime(jsonObject2.optString("displayTime"));
                                sclm.setAreaManagerId(jsonObject2.optString("areaManagerId"));
                                sclm.setAreaManagerName(jsonObject2.optString("areaManagerName"));
                                sclm.setAreaManagerNumber(jsonObject2.optString("areaManagerNumber"));

                                serviceCenterListModels.add(sclm);
                            }

                            sst_recyclerview.setAdapter(serviceCenterListAdapter);
                            serviceCenterListAdapter.notifyDataSetChanged();

                        } else {
                            GlobalCalls.showToast("No Data Available", ServiceCenterListActivity.this);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ServiceCenterListActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(ServiceCenterListActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }
}
