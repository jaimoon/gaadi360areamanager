package in.g360.gaadi360areamanager.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.widget.SearchView;

import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import in.g360.gaadi360areamanager.MainActivity;
import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.adapters.AreaManagerNameAdapter;
import in.g360.gaadi360areamanager.adapters.TrackingAdapter;
import in.g360.gaadi360areamanager.models.TrackingModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.GlobalCalls;
import in.g360.gaadi360areamanager.utils.NetworkChecking;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class TrackingActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close;
    TextView toolbar_title, name_txt, date_txt;
    Button submit_btn;
    Typeface regular, bold;
    UserSessionManager userSessionManager;

    SearchView track_search;
    RecyclerView track_recyclerview;
    AreaManagerNameAdapter areaManagerNameAdapter;
    TrackingAdapter trackingAdapter;
    ArrayList<TrackingModel> trackingModels = new ArrayList<TrackingModel>();
    String accessToken, areaManagerId,userName,mobile;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        checkInternet = NetworkChecking.isConnected(this);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        name_txt = findViewById(R.id.name_txt);
        name_txt.setTypeface(bold);
        name_txt.setOnClickListener(this);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(bold);
        date_txt.setOnClickListener(this);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setTypeface(bold);
        submit_btn.setOnClickListener(this);

        track_recyclerview = findViewById(R.id.track_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        track_recyclerview.setLayoutManager(layoutManager);
        trackingAdapter = new TrackingAdapter(trackingModels, TrackingActivity.this, R.layout.row_tracking);

        getDetails();

        track_search = findViewById(R.id.track_search);
        EditText searchEditText = track_search.findViewById(androidx.appcompat.R.id.search_src_text);
        track_search.setOnClickListener(v -> track_search.setIconified(false));
        track_search.setOnQueryTextListener(new androidx.appcompat.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                trackingAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getDetails() {
        String url = AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LIST;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                trackingModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.optJSONObject("data");

                        JSONArray jsonArray = jsonObject1.optJSONArray("areaManagerLocation");

                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                TrackingModel tm = new TrackingModel();
                                tm.setId(jsonObject2.optString("id"));
                                tm.setUserId(jsonObject2.optString("userId"));
                                tm.setUserName(jsonObject2.optString("userName"));
                                tm.setCreatedTime(jsonObject2.optString("createdTime"));
                                tm.setLatitude(jsonObject2.optString("latitude"));
                                tm.setLongitude(jsonObject2.optString("longitude"));
                                tm.setLocation(jsonObject2.optString("location"));
                                tm.setMobile(jsonObject2.optString("mobile"));
                                userName = jsonObject2.optString("userName");
                                mobile = jsonObject2.optString("mobile");

                                trackingModels.add(tm);
                            }

                            track_recyclerview.setAdapter(trackingAdapter);
                            trackingAdapter.notifyDataSetChanged();

                        } else {
                            GlobalCalls.showToast("No Data Available", TrackingActivity.this);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(TrackingActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(TrackingActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == name_txt) {
            if (checkInternet) {

                //name_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.black));
                //name_txt.setBackgroundResource(R.drawable.rounded_border);

                getNames();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == date_txt) {
            if (checkInternet) {

                //date_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.black));
                //date_txt.setBackgroundResource(R.drawable.rounded_border);

                getDate();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == submit_btn) {
            if (checkInternet) {

                getSpecificDetails();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void getNames() {
        String url = AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LIST;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                trackingModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.optJSONObject("data");

                        JSONArray jsonArray = jsonObject1.optJSONArray("areaManagerLocation");

                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                TrackingModel tm = new TrackingModel();
                                tm.setId(jsonObject2.optString("id"));
                                tm.setUserId(jsonObject2.optString("userId"));
                                tm.setUserName(jsonObject2.optString("userName"));
                                tm.setCreatedTime(jsonObject2.optString("createdTime"));
                                tm.setLatitude(jsonObject2.optString("latitude"));
                                tm.setLongitude(jsonObject2.optString("longitude"));
                                tm.setLocation(jsonObject2.optString("location"));
                                tm.setMobile(jsonObject2.optString("mobile"));
                                userName = jsonObject2.optString("userName");
                                mobile = jsonObject2.optString("mobile");

                                trackingModels.add(tm);
                            }

                            track_recyclerview.setAdapter(trackingAdapter);
                            trackingAdapter.notifyDataSetChanged();

                            areaManagerNameDialog();


                        } else {
                            GlobalCalls.showToast("No Data Available", TrackingActivity.this);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(TrackingActivity.this);
        requestQueue.add(stringRequest);
    }

    private void areaManagerNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
        LayoutInflater inflater = TrackingActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.area_manager_name_dialog, null);

        TextView name_title = dialog_layout.findViewById(R.id.name_title);
        name_title.setTypeface(regular);

        final SearchView name_search = dialog_layout.findViewById(R.id.name_search);
        EditText searchEditText = name_search.findViewById(androidx.appcompat.R.id.search_src_text);

        RecyclerView name_recyclerview = dialog_layout.findViewById(R.id.name_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        name_recyclerview.setLayoutManager(layoutManager);

        areaManagerNameAdapter = new AreaManagerNameAdapter(trackingModels, TrackingActivity.this, R.layout.row_names);
        name_recyclerview.setAdapter(areaManagerNameAdapter);

        name_search.setOnClickListener(v -> name_search.setIconified(false));

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog = builder.create();

        name_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                areaManagerNameAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    public void setAreaManagerName(String id, String name,String areamobile) {

        dialog.dismiss();
        areaManagerNameAdapter.notifyDataSetChanged();

        areaManagerId = id;
        userName = name;
        mobile = areamobile;
        name_txt.setText(name);
        name_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.white));
        name_txt.setBackgroundResource(R.drawable.rounded_border_btn);

        getDate();

    }

    public void getDate() {

        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String strDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        date_txt.setText(strDate);

                        date_txt.setTextColor(ContextCompat.getColor(TrackingActivity.this, R.color.white));
                        date_txt.setBackgroundResource(R.drawable.rounded_border_btn);

                    }

                }, year, month, day);

        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void getSpecificDetails() {

        String url = AppUrls.BASE_URL + AppUrls.AREA_MANAGER_LOCATION_DETAILS + areaManagerId + AppUrls.AREA_MANAGER_LOCATION_DETAILS2 +
                date_txt.getText().toString();

        Log.d("URL",url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                trackingModels.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equalsIgnoreCase("2000")) {

                        JSONObject jsonObject1 = jsonObject.optJSONObject("data");

                        JSONArray jsonArray = jsonObject1.optJSONArray("areaManagerLocation");

                        if (jsonArray.length() != 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                                TrackingModel tm = new TrackingModel();
                                tm.setId(jsonObject2.optString("id"));
                                tm.setUserId(jsonObject2.optString("userId"));
                                tm.setCreatedTime(jsonObject2.optString("createdTime"));
                                tm.setLatitude(jsonObject2.optString("latitude"));
                                tm.setLongitude(jsonObject2.optString("longitude"));
                                tm.setLocation(jsonObject2.optString("location"));
                                //tm.setUserName(jsonObject2.optString("userName"));
                                //tm.setMobile(jsonObject2.optString("mobile"));
                                tm.setUserName(userName);
                                tm.setMobile(mobile);

                                trackingModels.add(tm);
                            }

                            track_recyclerview.setAdapter(trackingAdapter);
                            trackingAdapter.notifyDataSetChanged();

                        } else {
                            GlobalCalls.showToast("No Data Available", TrackingActivity.this);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(TrackingActivity.this);
        requestQueue.add(stringRequest);
    }
}
