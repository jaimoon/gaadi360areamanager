package in.g360.gaadi360areamanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.TrackingActivity;
import in.g360.gaadi360areamanager.filters.AreaManagerNameSearch;
import in.g360.gaadi360areamanager.interfaces.TrackingItemClickListener;
import in.g360.gaadi360areamanager.models.TrackingModel;

public class AreaManagerNameAdapter extends RecyclerView.Adapter<AreaManagerNameAdapter.TrackingHolder> implements Filterable {

    public ArrayList<TrackingModel> trackingModels, filterList;
    TrackingActivity context;
    AreaManagerNameSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public AreaManagerNameAdapter(ArrayList<TrackingModel> trackingModels, TrackingActivity context, int resource) {
        this.trackingModels = trackingModels;
        this.filterList = trackingModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public TrackingHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_names, viewGroup, false);
        return new TrackingHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackingHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(trackingModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.trackingModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new AreaManagerNameSearch(filterList, this);
        }

        return filter;
    }

    public class TrackingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView area_manager_name_txt;
        TrackingItemClickListener trackingItemClickListener;


        TrackingHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            area_manager_name_txt = itemView.findViewById(R.id.area_manager_name_txt);


        }

        void bind(final TrackingModel trackingModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            area_manager_name_txt.setTypeface(bold);


            if (!trackingModel.getUserName().equalsIgnoreCase("null")) {
                area_manager_name_txt.setText(trackingModel.getUserName());
            } else {
                area_manager_name_txt.setText("No name");
            }

            itemView.setOnClickListener(view -> {
                context.setAreaManagerName(trackingModel.getUserId(), trackingModel.getUserName(),trackingModel.getMobile());
            });
        }

        @Override
        public void onClick(View view) {

            this.trackingItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
