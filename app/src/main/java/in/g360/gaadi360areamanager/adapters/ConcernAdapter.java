package in.g360.gaadi360areamanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.BookingDetailActivity;
import in.g360.gaadi360areamanager.activities.ConcernListActivity;
import in.g360.gaadi360areamanager.filters.ConcernSearch;
import in.g360.gaadi360areamanager.interfaces.ConcernItemClickListener;
import in.g360.gaadi360areamanager.models.ConcernModel;
import in.g360.gaadi360areamanager.utils.AppUrls;
import in.g360.gaadi360areamanager.utils.GlobalCalls;
import in.g360.gaadi360areamanager.utils.UserSessionManager;

public class ConcernAdapter extends RecyclerView.Adapter<ConcernAdapter.ConcernHolder> implements Filterable {

    public ArrayList<ConcernModel> concernModels, filterList;
    Activity context;
    ConcernSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    EditText concern_edt;
    String accessToken;
    UserSessionManager userSessionManager;

    public ConcernAdapter(ArrayList<ConcernModel> concernModels, Activity context, int resource) {
        this.concernModels = concernModels;
        this.filterList = concernModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ConcernHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_concern, viewGroup, false);
        return new ConcernHolder(view);
    }

    @Override
    public void onBindViewHolder(ConcernHolder concernHolder, final int position) {

        setAnimation(concernHolder.itemView, position);
        concernHolder.bind(concernModels.get(position));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

    }

    @Override
    public int getItemCount() {
        return this.concernModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ConcernSearch(filterList, this);
        }

        return filter;
    }

    public class ConcernHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ConcernItemClickListener concernItemClickListener;
        TextView booking_id_txt, sst_txt, desp_txt, res_txt, user_number_title_txt, user_number_txt, time_txt, desp_title, res_title;
        Button resolve_btn;
        String concernId, bookingId, sstName, desp, resolveMessage, userMobile, bookingDate, bookingTime, status;

        ConcernHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            booking_id_txt = itemView.findViewById(R.id.booking_id_txt);
            sst_txt = itemView.findViewById(R.id.sst_txt);
            desp_txt = itemView.findViewById(R.id.desp_txt);
            res_txt = itemView.findViewById(R.id.res_txt);
            user_number_title_txt = itemView.findViewById(R.id.user_number_title_txt);
            user_number_txt = itemView.findViewById(R.id.user_number_txt);
            time_txt = itemView.findViewById(R.id.time_txt);
            resolve_btn = itemView.findViewById(R.id.resolve_btn);
            desp_title = itemView.findViewById(R.id.desp_title);
            res_title = itemView.findViewById(R.id.res_title);

        }

        void bind(final ConcernModel concernModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            booking_id_txt.setTypeface(bold);
            sst_txt.setTypeface(bold);
            desp_txt.setTypeface(regular);
            res_txt.setTypeface(regular);
            user_number_title_txt.setTypeface(bold);
            user_number_txt.setTypeface(bold);
            time_txt.setTypeface(regular);
            desp_title.setTypeface(bold);
            res_title.setTypeface(bold);

            concernId = concernModel.getConcernId();
            bookingId = concernModel.getBookingId();
            sstName = concernModel.getServiceCenterName();
            desp = concernModel.getConcernMessage();
            resolveMessage = concernModel.getResolvedMessage();
            userMobile = concernModel.getUserMobile();
            bookingDate = concernModel.getBookingDate();
            bookingTime = concernModel.getBookingTime();
            status = concernModel.getStatus();

            if (status.equalsIgnoreCase("1")) {
                resolve_btn.setText("Resolve");
                resolve_btn.setBackgroundResource(R.drawable.orange_rounded_button);

                resolve_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showConcernAlertDialog(R.layout.dialog_concern_layout, concernId);
                    }
                });

            } else {
                resolve_btn.setText("Resolved");
                resolve_btn.setBackgroundResource(R.drawable.green_rounded_button);

            }

            booking_id_txt.setText("Booking Id : " + bookingId);
            sst_txt.setText(sstName);
            desp_txt.setText(desp);
            if (!resolveMessage.equalsIgnoreCase("null")) {
                res_title.setVisibility(View.VISIBLE);
                res_txt.setText(resolveMessage);
            } else {
                res_title.setVisibility(View.GONE);
                res_txt.setVisibility(View.GONE);
            }
            user_number_txt.setPaintFlags(user_number_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            user_number_txt.setText(userMobile);
            String resultDate = convertStringDateToAnotherStringDate(concernModel.getBookingDate(), "yyyy-MM-dd", "dd-MM-yyyy");
            Log.d("ResultDate", resultDate);

            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(bookingTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, 5);
            cal.add(Calendar.MINUTE, 30);
            String newTime = df.format(cal.getTime());

            try {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                Date date = dateFormatter.parse(newTime);
                SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                newTime = timeFormatter.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            time_txt.setText("Booking Time " + resultDate + " " + newTime);

            user_number_title_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(userMobile)));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);

                }
            });

            user_number_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(userMobile)));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);

                }
            });


            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(context, BookingDetailActivity.class);
                intent.putExtra("activity", "ConcernAdapter");
                intent.putExtra("bookingId", bookingId);
                context.startActivity(intent);
            });
        }

        @Override
        public void onClick(View view) {

            this.concernItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void showConcernAlertDialog(int layout, String concernId) {
        dialogBuilder = new AlertDialog.Builder(context);
        View layoutView = context.getLayoutInflater().inflate(layout, null);
        concern_edt = layoutView.findViewById(R.id.concern_edt);
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        Button submit_btn = layoutView.findViewById(R.id.submit_btn);
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!concern_edt.getText().toString().equalsIgnoreCase("")) {
                    sendConcern(concernId);
                } else {
                    concern_edt.setError("Please Enter Concern");
                }

            }
        });
        alertDialog.setCancelable(false);
    }

    private void sendConcern(String concernId) {
        String url = AppUrls.BASE_URL + AppUrls.RESOLVE_CONCERN;

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("concernId", concernId);
            jsonObj.put("resolvedMessage", concern_edt.getText().toString());

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("ConcernStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {
                        alertDialog.dismiss();
                        GlobalCalls.showToast("Your Concern Submitted Successfully..!", context);

                        Intent intent = new Intent(context, ConcernListActivity.class);
                        context.startActivity(intent);

                    } else {
                        GlobalCalls.showToast("Something went wrong..!", context);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
