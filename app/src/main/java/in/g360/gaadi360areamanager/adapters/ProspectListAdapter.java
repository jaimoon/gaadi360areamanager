package in.g360.gaadi360areamanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.ProspectListActivity;
import in.g360.gaadi360areamanager.activities.ServiceCenterListActivity;
import in.g360.gaadi360areamanager.filters.ProspectListSearch;
import in.g360.gaadi360areamanager.filters.ServiceCenterListSearch;
import in.g360.gaadi360areamanager.interfaces.ProspectListItemClickListener;
import in.g360.gaadi360areamanager.interfaces.SstItemClickListener;
import in.g360.gaadi360areamanager.models.ProspectListModel;
import in.g360.gaadi360areamanager.models.ServiceCenterListModel;
import in.g360.gaadi360areamanager.utils.DateTimeUtils;

public class ProspectListAdapter extends RecyclerView.Adapter<ProspectListAdapter.ProspectListHolder> implements Filterable {

    public ArrayList<ProspectListModel> prospectListModels, filterList;
    ProspectListActivity context;
    ProspectListSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public ProspectListAdapter(ArrayList<ProspectListModel> prospectListModels, ProspectListActivity context, int resource) {
        this.prospectListModels = prospectListModels;
        this.filterList = prospectListModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ProspectListHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_prospects, viewGroup, false);
        return new ProspectListHolder(view);
    }

    @Override
    public void onBindViewHolder(ProspectListHolder prospectListHolder, final int position) {

        setAnimation(prospectListHolder.itemView, position);
        prospectListHolder.bind(prospectListModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.prospectListModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ProspectListSearch(filterList, this);
        }

        return filter;
    }

    public class ProspectListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView call_img, email_img, am_call_img;
        TextView name_txt, booking_date_txt, mobile_txt, email_txt, comments_txt, am_name_txt, am_mobile_txt;
        ProspectListItemClickListener prospectListItemClickListener;


        ProspectListHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            call_img = itemView.findViewById(R.id.call_img);
            email_img = itemView.findViewById(R.id.email_img);
            am_call_img = itemView.findViewById(R.id.am_call_img);
            name_txt = itemView.findViewById(R.id.name_txt);
            booking_date_txt = itemView.findViewById(R.id.booking_date_txt);
            mobile_txt = itemView.findViewById(R.id.mobile_txt);
            email_txt = itemView.findViewById(R.id.email_txt);
            comments_txt = itemView.findViewById(R.id.comments_txt);
            am_name_txt = itemView.findViewById(R.id.am_name_txt);
            am_mobile_txt = itemView.findViewById(R.id.am_mobile_txt);

        }

        void bind(final ProspectListModel prospectListModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            name_txt.setTypeface(bold);
            booking_date_txt.setTypeface(bold);
            comments_txt.setTypeface(regular);
            am_name_txt.setTypeface(regular);
            mobile_txt.setTypeface(regular);
            mobile_txt.setPaintFlags(mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            email_txt.setTypeface(regular);
            email_txt.setPaintFlags(email_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            am_mobile_txt.setTypeface(regular);
            am_mobile_txt.setPaintFlags(am_mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);



            /*if (!prospectListModel.getFirstName().equalsIgnoreCase("null") || !prospectListModel.getLastName().equalsIgnoreCase("null")) {
                name_txt.setText(prospectListModel.getFirstName() + " " + prospectListModel.getLastName());
            } else {
                name_txt.setText("No name");
            }*/

            name_txt.setText(prospectListModel.getFirstName() + " " + prospectListModel.getLastName());

            if (!prospectListModel.getBookingDate().equalsIgnoreCase("null")) {
                String resultDate = DateTimeUtils.formatDate(prospectListModel.getBookingDate(), "yyyy-MM-dd", "dd-MM-yyyy");
                booking_date_txt.setText("Schedule Booking - " + resultDate);
            } else {
                booking_date_txt.setText("No Date");
            }
            if (!prospectListModel.getComments().equalsIgnoreCase("null")) {
                comments_txt.setText(prospectListModel.getComments());
            } else {
                comments_txt.setText("Not available");
            }

            if (!prospectListModel.getUsername().isEmpty()) {
                call_img.setVisibility(View.VISIBLE);
                mobile_txt.setVisibility(View.VISIBLE);
                mobile_txt.setText(prospectListModel.getUsername());
            } else {
                call_img.setVisibility(View.GONE);
                mobile_txt.setVisibility(View.GONE);
            }

            if (!prospectListModel.getEmail().equalsIgnoreCase("null")) {
                email_img.setVisibility(View.VISIBLE);
                email_txt.setVisibility(View.VISIBLE);
                email_txt.setText(prospectListModel.getEmail());
            } else {
                email_img.setVisibility(View.GONE);
                email_txt.setVisibility(View.GONE);
            }

            if (!prospectListModel.getAreaManagerName().equalsIgnoreCase("")) {
                am_name_txt.setVisibility(View.VISIBLE);
                am_name_txt.setText(prospectListModel.getAreaManagerName() + "  (Area Manager)");
            } else {
                am_name_txt.setVisibility(View.GONE);
                am_name_txt.setText("No name");
            }

            if (!prospectListModel.getAreaManagerNumber().isEmpty()) {
                am_call_img.setVisibility(View.VISIBLE);
                am_mobile_txt.setVisibility(View.VISIBLE);
                am_mobile_txt.setText(prospectListModel.getAreaManagerNumber());
            } else {
                am_call_img.setVisibility(View.GONE);
                am_mobile_txt.setVisibility(View.GONE);
            }

            mobile_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(prospectListModel.getUsername())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);
                }
            });

            email_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{prospectListModel.getEmail()});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Text");
                    context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                }
            });

            am_mobile_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(prospectListModel.getAreaManagerNumber())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);
                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.prospectListItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
