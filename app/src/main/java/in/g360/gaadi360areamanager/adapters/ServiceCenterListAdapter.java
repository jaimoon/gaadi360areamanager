package in.g360.gaadi360areamanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.ServiceCenterListActivity;
import in.g360.gaadi360areamanager.activities.TrackingActivity;
import in.g360.gaadi360areamanager.filters.ServiceCenterListSearch;
import in.g360.gaadi360areamanager.filters.TrackingSearch;
import in.g360.gaadi360areamanager.interfaces.SstItemClickListener;
import in.g360.gaadi360areamanager.interfaces.TrackingItemClickListener;
import in.g360.gaadi360areamanager.models.ServiceCenterListModel;
import in.g360.gaadi360areamanager.models.TrackingModel;

public class ServiceCenterListAdapter extends RecyclerView.Adapter<ServiceCenterListAdapter.ServiceCenterListHolder> implements Filterable {

    public ArrayList<ServiceCenterListModel> serviceCenterListModels, filterList;
    ServiceCenterListActivity context;
    ServiceCenterListSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public ServiceCenterListAdapter(ArrayList<ServiceCenterListModel> serviceCenterListModels, ServiceCenterListActivity context, int resource) {
        this.serviceCenterListModels = serviceCenterListModels;
        this.filterList = serviceCenterListModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ServiceCenterListHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sst, viewGroup, false);
        return new ServiceCenterListHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceCenterListHolder serviceCenterListHolder, final int position) {

        setAnimation(serviceCenterListHolder.itemView, position);
        serviceCenterListHolder.bind(serviceCenterListModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.serviceCenterListModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ServiceCenterListSearch(filterList, this);
        }

        return filter;
    }

    public class ServiceCenterListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView redirect_img,am_call_img;
        TextView sst_name_txt, name_txt, mobile_txt, email_txt, location_txt, time_txt, am_name_txt, am_mobile_txt;
        RatingBar sst_rating;
        SstItemClickListener sstItemClickListener;


        ServiceCenterListHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            redirect_img = itemView.findViewById(R.id.redirect_img);
            am_call_img = itemView.findViewById(R.id.am_call_img);
            sst_name_txt = itemView.findViewById(R.id.sst_name_txt);
            name_txt = itemView.findViewById(R.id.name_txt);
            location_txt = itemView.findViewById(R.id.location_txt);
            time_txt = itemView.findViewById(R.id.time_txt);
            mobile_txt = itemView.findViewById(R.id.mobile_txt);
            email_txt = itemView.findViewById(R.id.email_txt);
            sst_rating = itemView.findViewById(R.id.sst_rating);
            am_name_txt = itemView.findViewById(R.id.am_name_txt);
            am_mobile_txt = itemView.findViewById(R.id.am_mobile_txt);

        }

        void bind(final ServiceCenterListModel serviceCenterListModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            am_name_txt.setTypeface(bold);
            sst_name_txt.setTypeface(bold);
            name_txt.setTypeface(bold);
            location_txt.setTypeface(regular);
            time_txt.setTypeface(regular);
            mobile_txt.setTypeface(regular);
            mobile_txt.setPaintFlags(mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            email_txt.setTypeface(regular);
            email_txt.setPaintFlags(email_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            am_mobile_txt.setTypeface(regular);
            am_mobile_txt.setPaintFlags(am_mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            if (!serviceCenterListModel.getServiceCenterName().equalsIgnoreCase("null")) {
                sst_name_txt.setText(serviceCenterListModel.getServiceCenterName());
            } else {
                sst_name_txt.setText("No name");
            }
            if (!serviceCenterListModel.getContactName().equalsIgnoreCase("null")) {
                name_txt.setText(serviceCenterListModel.getContactName());
            } else {
                name_txt.setText("No name");
            }
            if (!serviceCenterListModel.getAddress().equalsIgnoreCase("null")) {
                location_txt.setText(serviceCenterListModel.getAddress());
            } else {
                location_txt.setText("Location unavailable");
            }
            if (!serviceCenterListModel.getDisplayTime().equalsIgnoreCase("null")) {
                time_txt.setText(serviceCenterListModel.getDisplayTime());
            } else {
                time_txt.setText("Not found");
            }
            if (!serviceCenterListModel.getContactMobileNumber().equalsIgnoreCase("null")) {
                mobile_txt.setText(serviceCenterListModel.getContactMobileNumber());
            } else {
                mobile_txt.setText("Not found");
            }
            if (!serviceCenterListModel.getContactEmail().equalsIgnoreCase("null")) {
                email_txt.setText(serviceCenterListModel.getContactEmail());
            } else {
                email_txt.setText("Not found");
            }
            if (!serviceCenterListModel.getAverageRating().equalsIgnoreCase("null")) {
                sst_rating.setRating(Float.parseFloat(serviceCenterListModel.getAverageRating()));
            } else {
                email_txt.setText("Not found");
            }

            if (!serviceCenterListModel.getAreaManagerName().equalsIgnoreCase("")) {
                am_name_txt.setVisibility(View.VISIBLE);
                am_name_txt.setText(serviceCenterListModel.getAreaManagerName() + "  (Area Manager)");
            } else {
                am_name_txt.setVisibility(View.GONE);
                am_name_txt.setText("No name");
            }

            if (!serviceCenterListModel.getAreaManagerNumber().isEmpty()) {
                am_call_img.setVisibility(View.VISIBLE);
                am_mobile_txt.setVisibility(View.VISIBLE);
                am_mobile_txt.setText(serviceCenterListModel.getAreaManagerNumber());
            } else {
                am_call_img.setVisibility(View.GONE);
                am_mobile_txt.setVisibility(View.GONE);
                am_mobile_txt.setText("No found");
            }

            mobile_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(serviceCenterListModel.getContactMobileNumber())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);
                }
            });

            email_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("plain/text");
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{serviceCenterListModel.getContactEmail()});
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");
                    context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                }
            });

            redirect_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String uri = "http://maps.google.com/maps?q=loc:" + serviceCenterListModel.getLatitude() + "," + serviceCenterListModel.getLongitude() + " (" + "Area Managet" + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);

                }
            });

            am_mobile_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(serviceCenterListModel.getAreaManagerNumber())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);
                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.sstItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
