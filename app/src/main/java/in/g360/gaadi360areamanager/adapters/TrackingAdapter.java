package in.g360.gaadi360areamanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.apache.commons.codec.binary.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.TrackingActivity;
import in.g360.gaadi360areamanager.filters.TrackingSearch;
import in.g360.gaadi360areamanager.interfaces.TrackingItemClickListener;
import in.g360.gaadi360areamanager.models.TrackingModel;

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.TrackingHolder> implements Filterable {

    public ArrayList<TrackingModel> trackingModels, filterList;
    TrackingActivity context;
    TrackingSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public TrackingAdapter(ArrayList<TrackingModel> trackingModels, TrackingActivity context, int resource) {
        this.trackingModels = trackingModels;
        this.filterList = trackingModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public TrackingHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_tracking, viewGroup, false);
        return new TrackingHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackingHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(trackingModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.trackingModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new TrackingSearch(filterList, this);
        }

        return filter;
    }

    public class TrackingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView redirect_img;
        TextView name_txt, mobile_txt, location_txt, time_txt;
        TrackingItemClickListener trackingItemClickListener;


        TrackingHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            redirect_img = itemView.findViewById(R.id.redirect_img);
            name_txt = itemView.findViewById(R.id.name_txt);
            location_txt = itemView.findViewById(R.id.location_txt);
            time_txt = itemView.findViewById(R.id.time_txt);
            mobile_txt = itemView.findViewById(R.id.mobile_txt);

        }

        void bind(final TrackingModel trackingModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            name_txt.setTypeface(bold);
            location_txt.setTypeface(regular);
            time_txt.setTypeface(regular);
            mobile_txt.setTypeface(regular);
            mobile_txt.setPaintFlags(mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            if (!trackingModel.getUserName().equalsIgnoreCase("null")) {
                name_txt.setText(trackingModel.getUserName());
            } else {
                name_txt.setText("No name");
            }
            if (!trackingModel.getLocation().equalsIgnoreCase("null")) {
                location_txt.setText(trackingModel.getLocation());
            } else {
                location_txt.setText("Location unavailable");
            }
            if (!trackingModel.getCreatedTime().equalsIgnoreCase("null")) {
                time_txt.setText(trackingModel.getCreatedTime());
            } else {
                time_txt.setText("Not found");
            }
            if (!trackingModel.getMobile().equalsIgnoreCase("null")) {
                mobile_txt.setText(trackingModel.getMobile());
            } else {
                mobile_txt.setText("Not found");
            }

            mobile_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" + Uri.encode(trackingModel.getMobile())));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);
                }
            });

            redirect_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String uri = "http://maps.google.com/maps?q=loc:" + trackingModel.getLatitude() + "," + trackingModel.getLongitude() + " (" + "Area Managet" + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);

                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.trackingItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
