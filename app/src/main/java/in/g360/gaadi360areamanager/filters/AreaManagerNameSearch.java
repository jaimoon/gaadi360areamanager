package in.g360.gaadi360areamanager.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.adapters.AreaManagerNameAdapter;
import in.g360.gaadi360areamanager.models.TrackingModel;

public class AreaManagerNameSearch extends Filter {

    AreaManagerNameAdapter adapter;
    ArrayList<TrackingModel> filterList;

    public AreaManagerNameSearch(ArrayList<TrackingModel> filterList, AreaManagerNameAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<TrackingModel> filteredPlayers = new ArrayList<TrackingModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getUserName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.trackingModels = (ArrayList<TrackingModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
