package in.g360.gaadi360areamanager.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.adapters.ConcernAdapter;
import in.g360.gaadi360areamanager.models.ConcernModel;

public class ConcernSearch extends Filter {

    ConcernAdapter adapter;
    ArrayList<ConcernModel> filterList;

    public ConcernSearch(ArrayList<ConcernModel> filterList, ConcernAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<ConcernModel> filteredPlayers = new ArrayList<ConcernModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceCenterName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getConcernId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getConcernMessage().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingDate().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getDeliveredTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getConcernTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUserMobile().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUserName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getFinalPrice().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.concernModels = (ArrayList<ConcernModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
