package in.g360.gaadi360areamanager.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.adapters.ProspectListAdapter;
import in.g360.gaadi360areamanager.models.ProspectListModel;


public class ProspectListSearch extends Filter {

    ProspectListAdapter adapter;
    ArrayList<ProspectListModel> filterList;

    public ProspectListSearch(ArrayList<ProspectListModel> filterList, ProspectListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<ProspectListModel> filteredPlayers = new ArrayList<ProspectListModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUsername().toUpperCase().contains(constraint) ||
                        filterList.get(i).getFirstName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLastName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingDate().toUpperCase().contains(constraint) ||
                        filterList.get(i).getComments().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCreatedTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getEmail().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAreaManagerName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAreaManagerNumber().toUpperCase().contains(constraint) ||
                        filterList.get(i).getType().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.prospectListModels = (ArrayList<ProspectListModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
