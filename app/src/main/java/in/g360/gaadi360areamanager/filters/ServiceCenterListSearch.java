package in.g360.gaadi360areamanager.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.adapters.ServiceCenterListAdapter;
import in.g360.gaadi360areamanager.models.ServiceCenterListModel;

public class ServiceCenterListSearch extends Filter {

    ServiceCenterListAdapter adapter;
    ArrayList<ServiceCenterListModel> filterList;

    public ServiceCenterListSearch(ArrayList<ServiceCenterListModel> filterList, ServiceCenterListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<ServiceCenterListModel> filteredPlayers = new ArrayList<ServiceCenterListModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceCenterId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getServiceCenterName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getContactName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUserId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getContactMobileNumber().toUpperCase().contains(constraint) ||
                        filterList.get(i).getContactEmail().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAddress().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLatitude().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLongitude().toUpperCase().contains(constraint) ||
                        filterList.get(i).getOpenTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCloseTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAverageRating().toUpperCase().contains(constraint) ||
                        filterList.get(i).getDisplayTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAreaManagerName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAreaManagerNumber().toUpperCase().contains(constraint) ||
                        filterList.get(i).getAreaManagerId().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.serviceCenterListModels = (ArrayList<ServiceCenterListModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
