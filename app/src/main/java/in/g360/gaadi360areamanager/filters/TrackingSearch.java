package in.g360.gaadi360areamanager.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.g360.gaadi360areamanager.adapters.TrackingAdapter;
import in.g360.gaadi360areamanager.models.TrackingModel;

public class TrackingSearch extends Filter {

    TrackingAdapter adapter;
    ArrayList<TrackingModel> filterList;

    public TrackingSearch(ArrayList<TrackingModel> filterList, TrackingAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<TrackingModel> filteredPlayers = new ArrayList<TrackingModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUserId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getUserName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCreatedTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLatitude().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLongitude().toUpperCase().contains(constraint) ||
                        filterList.get(i).getLocation().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.trackingModels = (ArrayList<TrackingModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
