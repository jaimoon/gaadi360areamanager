package in.g360.gaadi360areamanager.interfaces;

import android.view.View;

public interface AddonHistoryItemClickListener
{
   void onItemClick(View v, int pos);
}
