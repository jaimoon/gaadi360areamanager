package in.g360.gaadi360areamanager.interfaces;

import android.view.View;

public interface InspectionItemClickListener
{
   void onItemClick(View v, int pos);
}
