package in.g360.gaadi360areamanager.models;

public class ProspectListModel {

    public String id;
    public String username;
    public String firstName;
    public String lastName;
    public String bookingDate;
    public String comments;
    public String email;
    public String createdTime;
    public String areaManagerName;
    public String areaManagerNumber;
    public String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getAreaManagerName() {
        return areaManagerName;
    }

    public void setAreaManagerName(String areaManagerName) {
        this.areaManagerName = areaManagerName;
    }

    public String getAreaManagerNumber() {
        return areaManagerNumber;
    }

    public void setAreaManagerNumber(String areaManagerNumber) {
        this.areaManagerNumber = areaManagerNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
