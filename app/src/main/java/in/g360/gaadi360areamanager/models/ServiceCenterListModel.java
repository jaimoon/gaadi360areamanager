package in.g360.gaadi360areamanager.models;

public class ServiceCenterListModel {

    public String serviceCenterId;
    public String serviceCenterName;
    public String contactName;
    public String userId;
    public String contactMobileNumber;
    public String contactEmail;
    public String address;
    public String latitude;
    public String longitude;
    public String openTime;
    public String closeTime;
    public String averageRating;
    public String deleted;
    public String createdTime;
    public String modifiedTime;
    public String displayTime;
    public String areaManagerId;
    public String areaManagerName;
    public String areaManagerNumber;

    public String getAreaManagerName() {
        return areaManagerName;
    }

    public void setAreaManagerName(String areaManagerName) {
        this.areaManagerName = areaManagerName;
    }

    public String getAreaManagerNumber() {
        return areaManagerNumber;
    }

    public void setAreaManagerNumber(String areaManagerNumber) {
        this.areaManagerNumber = areaManagerNumber;
    }

    public String getServiceCenterId() {
        return serviceCenterId;
    }

    public void setServiceCenterId(String serviceCenterId) {
        this.serviceCenterId = serviceCenterId;
    }

    public String getServiceCenterName() {
        return serviceCenterName;
    }

    public void setServiceCenterName(String serviceCenterName) {
        this.serviceCenterName = serviceCenterName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContactMobileNumber() {
        return contactMobileNumber;
    }

    public void setContactMobileNumber(String contactMobileNumber) {
        this.contactMobileNumber = contactMobileNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public String getAreaManagerId() {
        return areaManagerId;
    }

    public void setAreaManagerId(String areaManagerId) {
        this.areaManagerId = areaManagerId;
    }
}
