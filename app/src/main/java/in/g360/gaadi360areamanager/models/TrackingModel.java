package in.g360.gaadi360areamanager.models;

import in.g360.gaadi360areamanager.utils.DateTimeUtils;

public class TrackingModel {
    public String id;
    public String userId;
    public String userName;
    public String createdTime;
    public String latitude;
    public String longitude;
    public String location;
    public String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        createdTime = DateTimeUtils.utc("yyyy-MM-dd HH:mm:ss", createdTime, "dd-MM-yyyy hh:mm a");
        this.createdTime = createdTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
