package in.g360.gaadi360areamanager.utils;

public class AppUrls {

    public static String BASE_URL = "https://admin.gaadi360.com/gaadi2/api/";
    //public static String BASE_URL = "http://test.gaadi360.com/gaadi/api/";
    //public static String BASE_URL = "http://192.168.32.97:8080/gaadi/api/";

    public static String IMAGE_URL = BASE_URL + "media/file/display/";

    public static String VERSION_UPDATE = "build/info/1/3";
    public static String SERVICETYPE = "service/type";
    public static String CALL_OTP = "V2/login/resend/otp?mobile=";
    public static String PROFILE = "secure/profile/get";
    public static String EDIT_PROFILE = "secure/profile/update";
    public static String EDIT_PROFILE_PIC = "media/file/upload";
    public static String HELP_CENTER = "secure/help-center/get";
    public static String LOGIN = "login/generate/otp?mobile=";
    public static String VERIFYOTP = "login/via/otp?mobile=";
    public static String FCM = "secure/user/device";
    public static String SEND_LOCATION = "secure/location/save";
    public static String HISTORY = "secure/area_manager/bookings?status=";
    public static String HISTORY_DETAIL = "secure/booking/";
    public static String NOTIFICATIONS = "secure/notifications";
    public static String READ_UNREAD = "secure/notification/";
    public static String CLEAR_NOTIFICATIONS = "secure/notification/all/clear";
    public static String NOTIFICATION_COUNT = "secure/notifications/unread-count";
    public static String CONCERNS = "secure/area_manager/bookingConcerns?status=";
    public static String RESOLVE_CONCERN = "secure/booking/concern";
    public static String AREA_MANAGER_LIST = "secure/area-managers";
    public static String AREA_MANAGER_LOCATION_DETAILS = "secure/areaManager/";
    public static String AREA_MANAGER_LOCATION_DETAILS2 = "/location?searchDate=";
    public static String SST_LIST = "secure/areaManager/service_centers";
    public static String ADD_PROSPECT = "secure/prospect/create";
    public static String PROSPECTS_LIST = "secure/areaManager/prospects";


}

