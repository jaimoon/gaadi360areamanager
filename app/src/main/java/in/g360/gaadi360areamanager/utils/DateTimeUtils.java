package in.g360.gaadi360areamanager.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {

    public static String utc(String inputDateFormat, String time, String outputDateFormat) {

        try {
            SimpleDateFormat df = new SimpleDateFormat(inputDateFormat);
            Calendar cal = Calendar.getInstance();
            Date d = df.parse(time);
            cal.setTime(d);
            cal.add(Calendar.HOUR, 5);
            cal.add(Calendar.MINUTE, 30);
            String newTime = df.format(cal.getTime());
            SimpleDateFormat dateFormatter = new SimpleDateFormat(inputDateFormat);
            Date date = dateFormatter.parse(newTime);
            SimpleDateFormat timeFormatter = new SimpleDateFormat(outputDateFormat);
            return timeFormatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;

    }

    public static String formatDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }
}
