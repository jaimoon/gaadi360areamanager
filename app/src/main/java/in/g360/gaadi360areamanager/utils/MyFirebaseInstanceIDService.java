package in.g360.gaadi360areamanager.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import in.g360.gaadi360areamanager.R;
import in.g360.gaadi360areamanager.activities.SplashScreenActivity;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    PendingIntent pendingIntent;
    NotificationCompat.Builder builder;
    int NOTIFICATION_ID = 2020;
    NotificationManager notificationManager;
    long pattern[] = {100, 200, 300, 400, 500, 400, 300, 200, 400};

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String message = remoteMessage.getData().get("message");
        String imageUri = remoteMessage.getData().get("image");
        Log.d("NotCheck", "Remote"+"\n"+message + "\n" + imageUri);

        Log.d("Notification", remoteMessage.getNotification().getTitle() + "\n" + remoteMessage.getNotification().getBody());

        notificationManager = (NotificationManager) MyFirebaseInstanceIDService.this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(MyFirebaseInstanceIDService.this, SplashScreenActivity.class);
        pendingIntent = PendingIntent.getActivity(MyFirebaseInstanceIDService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap icon = BitmapFactory.decodeResource(MyFirebaseInstanceIDService.this.getResources(), R.drawable.app_icon);

        builder = new NotificationCompat.Builder(MyFirebaseInstanceIDService.this, "default")
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setLargeIcon(icon)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        if (remoteMessage.getNotification() != null) {

            Log.d("Notification", remoteMessage.getNotification().getBody());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel("in.g360.gaadi360areamanager", "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);

                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                channel.setVibrationPattern(pattern);

                builder.setChannelId("in.g360.gaadi360areamanager");

                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(channel);
                }
            }

            assert notificationManager != null;
            notificationManager.notify(NOTIFICATION_ID, builder.build());

        }

    }
}
